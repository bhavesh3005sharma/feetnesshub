package com.fitnesshub.com;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;

public class Helper {
    private static final String MY_PREFS_NAME = "FITNESS_HUB";

    public static void toast(Context context, String message) {
        Toast.makeText(context, message, Toast.LENGTH_LONG).show();
    }

    public static boolean isUserLoggedIn(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(Helper.MY_PREFS_NAME, Context.MODE_PRIVATE);
        return (sharedPreferences != null && sharedPreferences.getBoolean("isUserLoggedIn", false)
                && Prefs.getUser(context) != null && Prefs.getUser(context).getUsername() != null
                && FirebaseAuth.getInstance().getCurrentUser() != null);
    }

    public static void setFirstUse(Context context, ring bool) {
        Log.i("FirstChange ", bool + "");
        SharedPreferences.Editor editor = context.getSharedPreferences(Helper.MY_PREFS_NAME, Context.MODE_PRIVATE).edit();
        editor.putBoolean("first", bool);
        editor.apply();
    }

    public static boolean getFirstUse(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(Helper.MY_PREFS_NAME, Context.MODE_PRIVATE);
        Log.i("GetFirstChange ", sharedPreferences.getBoolean("first", true) + "");
        return sharedPreferences.getBoolean("first", true);
    }

    public static String getUserEmail(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(Helper.MY_PREFS_NAME, Context.MODE_PRIVATE);
        Log.i("user email ", sharedPreferences.getString("user_email", null));
        return sharedPreferences.getString("user_email", null));
    }
}
