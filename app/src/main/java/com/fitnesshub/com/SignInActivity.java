package com.fitnesshub.com;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Patterns;
import android.view.View;
import com.fitnesshub.com.databinding.ActivitySigninBinding;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class SignInActivity extends AppCompatActivity {

    FirebaseAuth mAuth = FirebaseAuth.getInstance();
    ActivitySigninBinding activitySigninBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        activitySigninBinding = ActivitySigninBinding.inflate(getLayoutInflater());
        setContentView(activitySigninBinding.getRoot());

        activitySigninBinding.button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String email_id = activitySigninBinding.textInputEmail.getText().toString();
                String password_str = activitySigninBinding.textInputPassword.getText().toString();

                if (email_id.isEmpty()) {
                    activitySigninBinding.textInputEmail.setError("Email should not be empty");
                    activitySigninBinding.textInputEmail.requestFocus();
                    return;
                }else if (!Patterns.EMAIL_ADDRESS.matcher(email_id).matches()) {
                    activitySigninBinding.textInputEmail.setError("Please enter a valid Email");
                    activitySigninBinding.textInputEmail.requestFocus();
                    return;
                }else{
                    activitySigninBinding.textInputEmail.setError(null);
                }
                
                if(password_str.isEmpty()) {
                    activitySigninBinding.textInputPassword.setError("Password should not be empty");
                    activitySigninBinding.textInputPassword.requestFocus();
                    return;
                }

                signIn(email_id, password_str);
            }
        });
    }

    public void signIn(String email_id, String password_str) {
        mAuth.signInWithEmailAndPassword(email_id, password_str).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()) {
//                    Prefs.setUserData(SignIn.this, user);
//                    Prefs.setUserLoggedIn(SignIn.this, true);

                    Intent intent = new Intent(SignInActivity.this, HomeActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                    finish();
                    Helper.toast(SignInActivity.this, "Signed In Success!!");
                } else Helper.toast(SignInActivity.this,task.getException().getMessage());
            }
        });
    }
}