package com.fitnesshub.com;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.provider.ContactsContract;
import android.util.Log;
import android.view.View;

import com.fitnesshub.com.databinding.ActivityHomeBinding;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.text.SimpleDateFormat;
import java.util.Date;

public class HomeActivity extends AppCompatActivity {

    ActivityHomeBinding activityHomeBinding;
    DatabaseReference database = FirebaseDatabase.getInstance().getReference();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityHomeBinding = ActivityHomeBinding.inflate(getLayoutInflater());
        setContentView(activityHomeBinding.getRoot());

        SimpleDateFormat ISO_8601_FORMAT = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:sss'Z'");

        String now = ISO_8601_FORMAT.format(new Date());

        database.child("yoga_class").get().addOnCompleteListener(new OnCompleteListener<DataSnapshot>() {
             @Override
             public void onComplete(@NonNull Task<DataSnapshot> task) {
                 if (task.isSuccessful()) {
                     DataSnapshot classSnapshot = task.getResult();
                     long capacity = (long)classSnapshot.child("capacity").getValue();
                     DataSnapshot participantsSnapshot = classSnapshot.child("participants");
                     DataSnapshot waitingListSnapshot = classSnapshot.child("waiting_list");
                     String classTime = (String) classSnapshot.child("time").getValue();
                     Date classDate = new Date(classTime);

                     activityHomeBinding.buttonJoinClassYoga.setText("Leave Class");
                     if(participantsSnapshot.hasChild(Helper.getUserEmail())){
                         activityHomeBinding.textviewStatusYoga.setText("Attending");
                     }else if(waitingListSnapshot.hasChild(Helper.getUserEmail())){
                         activityHomeBinding.textviewStatusYoga.setText("Waiting");
                     }else {
                         activityHomeBinding.textviewStatusYoga.setText("Not Registered");
                         activityHomeBinding.buttonJoinClassYoga.setText("Join Class");
                     }

                     // if class is going to start in 1/2 an hour & user is invited block his leave option
                     if(((new Date()).getTime() - classDate.getTime())/36e5<=60 &&
                             activityHomeBinding.buttonJoinClassYoga.getText().toString().equals("Leave Class")){
                         Log.i("TAG", "onComplete: button blocked");
                         activityHomeBinding.buttonJoinClassYoga.setEnabled(false);
                     }
                 } else {
                    Helper.toast(HomeActivity.this,"Oops Error in Syncing Class Status");
                 }
             }
         });

        activityHomeBinding.buttonJoinClassYoga.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                database.child("yoga_class").get().addOnCompleteListener(new OnCompleteListener<DataSnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<DataSnapshot> task) {
                        if(task.isSuccessful()){
                            DataSnapshot classSnapshot = task.getResult();
                            long capacity = (long)classSnapshot.child("capacity").getValue();
                            DataSnapshot participantsSnapshot = classSnapshot.child("participants");
                            DataSnapshot waitingListSnapshot = classSnapshot.child("waiting_list");
                            if(activityHomeBinding.buttonJoinClassYoga.getText().equals("Join Class")) {
                                if (participantsSnapshot.getChildrenCount() >= capacity) {
                                    Helper.toast(HomeActivity.this, "You are late.Class is Full. We are putting you in waiting list.");
                                    database.child("yoga_class").child("waiting_list").child(Helper.getUserEmail()).setValue(true);
                                } else {
                                    Helper.toast(HomeActivity.this, "Joined the Class.");
                                    database.child("yoga_class").child("participants").child(Helper.getUserEmail()).setValue(true);
                                }
                                activityHomeBinding.buttonJoinClassYoga.setText("Leave Class");
                            }else{
                                if(waitingListSnapshot.hasChild(Helper.getUserEmail())){
                                    database.child("yoga_class").child("waiting_list").child(Helper.getUserEmail()).removeValue();
                                }else if(participantsSnapshot.hasChild(Helper.getUserEmail())){
                                    database.child("yoga_class").child("participants").child(Helper.getUserEmail()).removeValue();

                                    // Now check for waiting list and move the first prson to participant list if list is not empty :)

                                    if(waitingListSnapshot.hasChildren()){
                                        String anotherUser = null;
                                        for(DataSnapshot d : waitingListSnapshot.getChildren()){
                                            anotherUser = d.getKey();
                                            break;
                                        }
                                        database.child("yoga_class").child("waiting_list").child(anotherUser).removeValue();
                                        database.child("yoga_class").child("participants").child(anotherUser).setValue(true);
                                    }
                                }
                                activityHomeBinding.buttonJoinClassYoga.setText("Join Class");
                            }
                        }else{
                            Helper.toast(HomeActivity.this,task.getException().getMessage());
                        }
                    }
                });
            }
        });

    }
}